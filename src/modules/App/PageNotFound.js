import React from 'react';

/**
 * Displays a Page Not Found (404) message.
 *
 */
const PageNotFound = () => {
    return (
        <div className="PageNotFound">
            <section>
                <h1>404</h1>
                <h2>Page not found</h2>
            </section>
        </div>
    );
};

export default PageNotFound;