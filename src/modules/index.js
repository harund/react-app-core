export {default as Account} from './Account';
export {default as App} from './App';
export {default as Auth} from './Auth';
export {default as Documents} from './Documents';
export {default as Home} from './Home';
export {default as PageNotFound} from './App/PageNotFound';