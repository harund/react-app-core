import axios from 'axios';

import ENV from '../../util/env-config';

/**
 * Responsible for API calls within Documents module.
 *
 * @class Service
 */
class Service {
    static requestConfig() {
        return {
            headers: {
                Authorization: `Bearer ${sessionStorage._sessionToken}`
            }
        };
    }

    static getData() {
        return axios.get(ENV.apiBase + '/posts', this.requestConfig());
    }

    static getDocument(id) {
        return axios.get(ENV.apiBase + '/posts/' + id, this.requestConfig());
    }

    static saveDocument(requestData) {
        return axios.put(ENV.apiBase + '/posts/' + requestData.id, requestData, this.requestConfig());
    }
}

export default Service;