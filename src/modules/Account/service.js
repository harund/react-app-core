import axios from 'axios';

import ENV from '../../util/env-config';

/**
 * Responsible for API calls within Account module.
 *
 * @class Service
 */
class Service {
    static requestConfig() {
        return {
            headers: {
                Authorization: `Bearer ${sessionStorage._sessionToken}`
            }
        };
    }

    static getUser(id) {
        return axios.get(ENV.apiBase + '/users/' + id, this.requestConfig());
    }

    static saveUser(requestData) {
        return axios.put(ENV.apiBase + '/users/' + requestData.id, requestData, this.requestConfig());
    }
}

export default Service;