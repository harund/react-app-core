- login page processing and storing the session X
- sidebar with menu and logout on the left side X
- api call on entering Home page X
- listing of results on Home page X
- detail view of a result on Home page (subpage/dialog/something else) X
- account page X
- editing account info on the account page X
- documents page X
- single document detail X
- nice 404 page X
- env files and configuration X
- error boundary component X
- refactor remaining components - remove constructors and use arrow functions for methods
- add tests
- cleanup, formatting, testing

"Work Sans",Arial,sans-serif