import React, {Component} from 'react';
import {Button, Form, Tab} from 'semantic-ui-react';

import {Header} from './../../components';
import Service from './service';
import './Account.css';

const setPanes = ({account, onChange}) => [
    {
        menuItem: 'General Info',
        pane: <Tab.Pane key="TP0">
                <Form>
                    <Form.Group widths='equal'>
                        <Form.Input
                            name='first_name'
                            value={account.first_name}
                            label='First name'
                            placeholder='First name'
                            onChange={onChange}/>
                        <Form.Input
                            name='last_name'
                            value={account.last_name}
                            label='Last name'
                            placeholder='Last name'
                            onChange={onChange}/>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Input
                            name='address'
                            value={account.address}
                            label='Address'
                            placeholder='Address'
                            onChange={onChange}/>
                        <Form.Input
                            name='city'
                            value={account.city}
                            label='City'
                            placeholder='City'
                            onChange={onChange}/>
                        <Form.Input
                            name='country'
                            value={account.country}
                            label='Country'
                            placeholder='Country'
                            onChange={onChange}/>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Input
                            name='phone'
                            value={account.phone}
                            label='Phone'
                            placeholder='Phone'
                            onChange={onChange}/>
                        <Form.Input
                            name='mobile'
                            value={account.mobile}
                            label='Mobile'
                            placeholder='Mobile'
                            onChange={onChange}/>
                        <Form.Input
                            name='local'
                            value={account.local}
                            label='Local'
                            placeholder='Local'
                            onChange={onChange}/>
                    </Form.Group>
                </Form>
            </Tab.Pane>
    }, {
        menuItem: 'Job details',
        pane: <Tab.Pane key="TP1">
                <Form>
                    <Form.Group widths='equal'>
                        <Form.Input
                            name='job_position'
                            value={account.job_position}
                            label='Position'
                            placeholder='Position'
                            onChange={onChange}/>
                        <Form.Input
                            name='department'
                            value={account.department}
                            label='Department'
                            placeholder='Department'
                            onChange={onChange}/>
                        <Form.Input
                            name='office_number'
                            value={account.office_number}
                            label='Office number'
                            placeholder='Office number'
                            onChange={onChange}/>
                    </Form.Group>
                </Form>
            </Tab.Pane>
    }, {
        menuItem: 'Profile Picture',
        pane: <Tab.Pane key="TP2">Profile picture</Tab.Pane>
    }, {
        menuItem: 'Password',
        pane: <Tab.Pane key="TP3">
                <Form>
                    <Form.Group widths='equal'>
                        <Form.Input
                            name='new_password'
                            value={account.new_password}
                            label='New password'
                            placeholder='New password'
                            type="password"
                            onChange={onChange}/>
                        <Form.Input
                            name='new_password_confirmation'
                            value={account.new_password_confirmation}
                            label='New password again'
                            placeholder='New password again'
                            type="password"
                            onChange={onChange}/>
                    </Form.Group>
                </Form>
            </Tab.Pane>
    }
];

/**
 * Displays user account information as a tabbed, editable form.
 *
 * @class Account
 * @extends {Component}
 */
class Account extends Component {
    state = {
        account: {}
    };

    componentDidMount() {
        Service
            .getUser(5)
            .then(result => {
                this.setState({account: result.data});
            })
            .catch(error => {
                console.error("Error getting user account.", error);
            });
    }

    handleFormChange = event => {
        let account = this.state.account;
        account[event.target.name] = event.target.value;
        this.setState({account});
    }

    handleSubmit = event => {
        event.preventDefault();
        Service
            .saveUser(this.state.account)
            .then(result => {
                this.setState({account: result.data});
            })
            .catch(error => {
                console.error("Error saving user account.", error);
            });
    }

    render() {
        return (
            <div className="Module Account-Module">
                <Header>
                    <h1 className="Module__Title">Account Settings</h1>
                </Header>
                <section>
                    <Tab
                        defaultActiveIndex={0}
                        renderActiveOnly={false}
                        panes={setPanes({account: this.state.account, onChange: this.handleFormChange})}/>
                    <Button
                        type="submit"
                        color="green"
                        size="large"
                        fluid
                        onClick={this.handleSubmit}>SAVE ACCOUNT DETAILS</Button>
                </section>
            </div>
        );
    }
}

export default Account;