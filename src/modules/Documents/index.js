import React, {Component} from 'react';
import ReactTable from 'react-table';
import {Button, Icon} from 'semantic-ui-react';
import 'react-table/react-table.css';

import {Header} from './../../components';
import DocumentDetailModal from './DocumentDetailModal';
import Service from './service';

// Column definition for react-table
const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id'
    }, {
        Header: 'Document title',
        accessor: 'title'
    }, {
        Header: 'Description',
        accessor: 'body'
    }, {
        Header: 'Created At',
        accessor: 'body'
    }, {
        Header: 'User responsible',
        accessor: 'body'
    }
];

/**
 * Displays table and detail views of documents.
 *
 * @class Documents
 * @extends {Component}
 */
class Documents extends Component {
    state = {
        data: [],
        document: {},
        isDocumentDetailVisible: false
    };

    componentDidMount() {
        Service
            .getData({})
            .then(result => {
                this.setState({data: result.data});
            })
            .catch(error => {
                console.error("Error getting documents.", error);
            });
    }

    handleResultClick = rowInfo => {
        if (!rowInfo.original || !rowInfo.original.id) {
            return;
        }

        Service
            .getDocument(rowInfo.original.id)
            .then(result => {
                this.setState({document: result.data, isDocumentDetailVisible: true});
            })
            .catch(error => {
                console.error("Error getting document detail.", error);
            });
    }

    handleHideDocumentDetail = () => {
        this.setState({isDocumentDetailVisible: false});
    }

    handleSaveDocumentDetail = document => {
        Service
            .saveDocument(document)
            .then(result => {
                this.setState({isDocumentDetailVisible: false});
            })
            .catch(error => {
                console.error("Error saving document detail.", error);
            });
    }

    renderDocumentDetailView = () => {
        if (this.state.isDocumentDetailVisible) {
            return <DocumentDetailModal
                open={this.state.isDocumentDetailVisible}
                document={this.state.document}
                onHide={this.handleHideDocumentDetail}
                onSave={this.handleSaveDocumentDetail}/>
        }
    }

    render() {
        return (
            <div className="Module Document-Module">
                <Header>
                    <h1 className="Module__Title">Document Overview</h1>

                    <Button
                        size="small"
                        color="green"
                        basic
                        circular
                        className="Module__Action-Button"
                        onClick={this.handleRefresh}><Icon name="refresh"/>Refresh</Button>
                </Header>
                <section>
                    <ReactTable
                        data={this.state.data}
                        showPageSizeOptions={false}
                        columns={COLUMNS}
                        getTdProps={(state, rowInfo, column, instance) => {
                        return {
                            onClick: e => {
                                this.handleResultClick(rowInfo)
                            }
                        }
                    }}/>
                </section>
                {this.renderDocumentDetailView()}
            </div>
        );
    }
}

export default Documents;