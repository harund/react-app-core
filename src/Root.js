import React, {Component} from 'react';
import {Route, Switch, Redirect, withRouter} from 'react-router-dom';

import {Auth, App} from './modules';

/**
 * Responsible for defining routes and handling access to public and private routes.
 * 
 * @class Root
 * @extends {Component}
 */
class Root extends Component {
    render() {
        return (
            <Switch>
                <PublicRoute exact path="/login" component={Auth}/>
                <PrivateRoute component={withRouter(App)}/>
            </Switch>
        );
    }
}

export default withRouter(Root);

/*
    Public and private routes
 */
const PublicRoute = ({
    component: Component,
    ...rest
}) => (
    <Route
        {...rest}
        render={props => (!sessionStorage._sessionToken
        ? (<Component {...props}/>)
        : (<Redirect exact to=''/>))}/>
);

const PrivateRoute = ({
    component: Component,
    ...rest
}) => (
    <Route
        {...rest}
        render={props => ((sessionStorage._sessionToken)
        ? (<Component {...props}/>)
        : (<Redirect to="/login"/>))}/>
);