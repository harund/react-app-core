const ENVIRONMENTS = {
    dev: {
        apiBase: "https://jsonplaceholder.typicode.com"
    },
    prod: {
        apiBase: "https://appprod.com"
    }
};

const ENV = ENVIRONMENTS['dev'];

export default ENV;
