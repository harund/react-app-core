import React, {Component} from 'react';
import {Switch, Route, NavLink, withRouter} from 'react-router-dom';
import {Icon, Sidebar, Menu, Dropdown} from 'semantic-ui-react';

import {Account, Home, Documents, PageNotFound} from './../../modules';
import './App.css';

const USER_MENU_TRIGGER = (
  <span>
    <Icon name="user"/>
    Name Surname
  </span>
);

const USER_MENU_OPTIONS = [
  {
    key: 'account',
    text: 'My Account',
    value: 'account'
  }, {
    key: 'logout',
    text: 'Log out',
    value: 'logout'
  }
];

/**
 * Shell of the private part of the application.
 * Displays a sidebar with menu and app content.
 *
 * @class App
 * @extends {Component}
 */
class App extends Component {
  handleLogout = event => {
    sessionStorage.removeItem('_sessionToken');
    this
      .props
      .history
      .push('/login');
  }

  handleUserMenuClick = (event, data) => {
    if (data.value === 'logout') {
      this.handleLogout();
      return;
    }

    this
      .props
      .history
      .push(`/${data.value}`);
  }

  render() {
    return (
      <div className="App-Module">
        <Sidebar.Pushable>
          <Sidebar
            as={Menu}
            className="App-Module-Sidebar"
            width="thin"
            visible={true}
            vertical
            borderless>
            <div className="brand">App</div>
            <Menu.Item fitted={true}>
              <NavLink exact activeClassName="--current-page" to="/">Home</NavLink>
            </Menu.Item>
            <Menu.Item fitted={true}>
              <NavLink exact activeClassName="--current-page" to="/documents">Documents</NavLink>
            </Menu.Item>

            <div className="App-Module-Sidebar__Bottom">
              <Menu.Item onClick={this.handleLogout}>Logout</Menu.Item>
            </div>
          </Sidebar>
          <Sidebar.Pusher>
            <div className="App-Content">
              <header id="headerRoot">
                <Dropdown
                  className="UserMenu"
                  trigger={USER_MENU_TRIGGER}
                  options={USER_MENU_OPTIONS}
                  value={''}
                  onChange={this.handleUserMenuClick}/>
              </header>

              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/documents" component={Documents}/>
                <Route exact path="/account" component={Account}/>
                <Route component={PageNotFound}/>
              </Switch>
            </div>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    );
  }
}

export default withRouter(App);
