import React, {Component} from 'react';
import ReactDOM from 'react-dom';

let headerRoot = document.getElementById('headerRoot');

/**
 * React Portal component used to display a header of each page with dynamic, page-specific content.
 *
 * @class Header
 * @extends {Component}
 */

class Header extends Component {
    el = document.createElement('span');

    componentDidMount() {
        headerRoot = document.getElementById('headerRoot')
        headerRoot.appendChild(this.el);
    }

    componentWillUnmount() {
        headerRoot = document.getElementById('headerRoot')
        headerRoot.removeChild(this.el);
    }

    render() {
        return ReactDOM.createPortal(this.props.children, this.el);
    }
}

export default Header;