import React, {Component} from 'react';
import {Button, Modal, Form} from 'semantic-ui-react';

/**
 * Displays details of item as an editable form.
 *
 * @class ItemDetailModal
 * @extends {Component}
 */
class ItemDetailModal extends Component {
    state = {
        item: this.props.item
    };

    handleChange = event => {
        let item = this.state.item;
        item[event.target.name] = event.target.value;
        this.setState({item});
    }

    render() {
        let {open, onHide, onSave} = this.props;

        return (
            <Modal open={open}>
                <Modal.Header>Item Detail</Modal.Header>
                <Modal.Content scrolling>
                    <Form>
                        <Form.Group widths='equal'>
                            <Form.Input
                                name='title'
                                value={this.state.item.title}
                                label='Title'
                                placeholder='Title'
                                onChange={this.handleChange}/>
                        </Form.Group>
                        <Form.Group inline>
                            <label>Status</label>
                            <Form.Radio
                                name='status'
                                label='Active'
                                value='active'
                                checked={false}
                                onChange={this.handleChange}/>
                            <Form.Radio
                                name='status'
                                label='Inactive'
                                value='inactive'
                                checked={false}
                                onChange={this.handleChange}/>
                        </Form.Group>
                        <Form.TextArea
                            name='body'
                            value={this.state.item.body}
                            label='Description'
                            placeholder='Additional item info...'/>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color="blue" floated="left" onClick={onHide}>
                        Close
                    </Button>

                    <Button color="green" onClick={() => onSave(this.state.item)}>
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ItemDetailModal;