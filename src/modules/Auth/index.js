import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Button, Form} from 'semantic-ui-react';

import './Auth.css';

/**
 * Displays a login form.
 *
 * @class Auth
 * @extends {Component}
 */
class Auth extends Component {
    state = {
        credentials: {
            email: '',
            password: ''
        },
        hasError: false
    };

    handleFormChange = event => {
        let credentials = this.state.credentials;
        credentials[event.target.name] = event.target.value;

        this.setState({credentials});
    }

    handleSubmit = event => {
        event.preventDefault();
        sessionStorage.setItem('_sessionToken', '__TOKEN__');
        this
            .props
            .history
            .push('/');
    }

    render() {
        let {email, password} = this.state.credentials;

        return (
            <div className="Auth-Module">
                <div className="Login-Wrapper">
                    <h1>Login</h1>
                    <Form>
                        <Form.Field>
                            <input
                                placeholder="email"
                                name="email"
                                value={email}
                                onChange={this.handleFormChange}/>
                        </Form.Field>
                        <Form.Field>
                            <input
                                placeholder="password"
                                type="password"
                                name="password"
                                value={password}
                                onChange={this.handleFormChange}/>
                        </Form.Field>
                        <Button
                            type="submit"
                            fluid
                            disabled={!email || !password}
                            onClick={this.handleSubmit}>LOGIN</Button>
                    </Form>
                </div>
            </div>
        );
    }
}

export default withRouter(Auth);