import React, {Component} from 'react';
import ReactTable from 'react-table';
import {Button, Icon} from 'semantic-ui-react';
import 'react-table/react-table.css';

import {Header, ErrorBoundary} from './../../components';
import ItemDetailModal from './ItemDetailModal';
import Service from './service';

// Column definition for react-table
const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        maxWidth: 100
    }, {
        Header: 'Title',
        accessor: 'title',
        maxWidth: 300
    }, {
        Header: 'Description',
        accessor: 'body',
        filterMethod: (filter, row) => {
            if (filter.value === "all") {
                return true;
            }
            if (filter.value === "true") {
                return row[filter.id] >= 21;
            }
            return row[filter.id] < 21;
        },
        Filter: ({filter, onChange}) => <select
                onChange={event => onChange(event.target.value)}
                style={{
                width: "100%"
            }}
                value={filter
                ? filter.value
                : "all"}>
                <option value="all">Show All</option>
                <option value="true">Active</option>
                <option value="false">Inactive</option>
            </select>
    }
];

/**
 * Displays table and detail views of data items.
 *
 * @class Home
 * @extends {Component}
 */
class Home extends Component {
    state = {
        data: [],
        itemDetail: {},
        isItemDetailVisible: false
    };

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        Service
            .getData({})
            .then(result => {
                this.setState({data: result.data});
            })
            .catch(error => {
                console.error("Error getting data.", error);
            });
    }

    handleRefresh = () => {
        this.getData();
    }

    showNewItemForm = () => {
        this.setState({
            itemDetail: {
                id: Math.round(Math.random() * 100)
            },
            isItemDetailVisible: true
        });
    }

    handleResultClick = rowInfo => {
        if (!rowInfo.original || !rowInfo.original.id) {
            return;
        }

        Service
            .getItem(rowInfo.original.id)
            .then(result => {
                this.setState({itemDetail: result.data, isItemDetailVisible: true});
            })
            .catch(error => {
                console.error("Error getting item detail.", error);
            });
    }

    handleHideItemDetail = () => {
        this.setState({isItemDetailVisible: false});
    }

    handleSaveItemDetail = (itemDetail) => {
        Service
            .saveItem(itemDetail)
            .then(result => {
                this.setState({isItemDetailVisible: false});
            })
            .catch(error => {
                console.error("Error saving item detail.", error);
            });
    }

    renderItemDetailView = () => {
        if (this.state.isItemDetailVisible) {
            return (<ItemDetailModal
                open={this.state.isItemDetailVisible}
                item={this.state.itemDetail}
                onHide={this.handleHideItemDetail}
                onSave={this.handleSaveItemDetail}/>);
        }
    }

    render() {
        let {data} = this.state;

        return (
            <div className="Module Home-Module">
                <Header>
                    <h1 className="Module__Title">Home Overview</h1>

                    <Button
                        size="small"
                        color="blue"
                        basic
                        circular
                        className="Module__Action-Button"
                        onClick={this.showNewItemForm}><Icon name="add"/>
                        New item</Button>

                    <Button
                        size="small"
                        color="green"
                        basic
                        circular
                        className="Module__Action-Button"
                        onClick={this.handleRefresh}><Icon name="refresh"/>Refresh</Button>
                </Header>
                <section>
                    <ErrorBoundary message="There was a problem displaying the data items table">
                        <ReactTable
                            data={data}
                            columns={COLUMNS}
                            showPageSizeOptions={false}
                            defaultPageSize={10}
                            noDataText="No data"
                            filterable
                            getTdProps={(state, rowInfo, column, instance) => {
                            return {
                                onClick: e => {
                                    this.handleResultClick(rowInfo)
                                }
                            };
                        }}/>
                    </ErrorBoundary>
                </section>
                {this.renderItemDetailView()}
            </div>
        );
    }
}

export default Home;