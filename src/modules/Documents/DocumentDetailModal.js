import React, {Component} from 'react';
import {Button, Modal, Form} from 'semantic-ui-react';

class DocumentDetailModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            document: this.props.document
        };

        this.handleChange = this
            .handleChange
            .bind(this);
    }

    handleChange(event) {
        let document = this.state.document;
        document[event.target.name] = event.target.value;
        this.setState({document});
    }

    render() {
        let {open, onHide, onSave} = this.props;

        return (
            <Modal open={open}>
                <Modal.Header>Item Detail</Modal.Header>
                <Modal.Content scrolling>
                    <Form>
                        <Form.Group widths='equal'>
                            <Form.Input
                                name='title'
                                value={this.state.document.title}
                                onChange={this.handleChange}
                                label='Document title'
                                placeholder='Document title'/>
                        </Form.Group>
                        <Form.TextArea
                            name='body'
                            value={this.state.document.body}
                            onChange={this.handleChange}
                            label='Extended description'
                            placeholder='Extended description of the document'/>
                        <Form.Group inline>
                            <Form.Radio
                                label='Is public'
                                name='is_public'
                                checked={this.state.document.is_public}
                                onChange={this.handleChange}/>
                        </Form.Group>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color="blue" floated="left" onClick={onHide}>
                        Close
                    </Button>

                    <Button color="green" onClick={() => onSave(this.state.document)}>
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default DocumentDetailModal;