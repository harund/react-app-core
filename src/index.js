import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom';
import {loadProgressBar} from 'axios-progress-bar';
import 'semantic-ui-css/semantic.min.css';

import Root from './Root';
import './style/global.css';

loadProgressBar({speed: 500}); // Sets and configures axios-progress-bar

ReactDOM.render(
    <BrowserRouter>
    <Root/>
</BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
