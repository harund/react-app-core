import React, {Component} from 'react';

/**
 * Displays a message instead of crash log in case od an error rendering the wrapped component.
 *
 * @class ErrorBoundary
 * @extends {Component}
 */
class ErrorBoundary extends Component {
    state = {
        error: null,
        errorInfo: null
    };

    componentDidCatch = (error, errorInfo) => {
        this.setState({error, errorInfo});
    }

    render() {
        if (this.state.errorInfo) {
            console.error("Error rendering component.", this.state.error, this.state.errorInfo);
            return (
                <p>
                    <strong>{this.props.message
                            ? this.props.message
                            : 'Error occured...'}</strong>
                </p>
            );
        }

        return this.props.children;
    }
}

export default ErrorBoundary;