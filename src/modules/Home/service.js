import axios from 'axios';

import ENV from '../../util/env-config';

/**
 * Responsible for API calls within Home module.
 *
 * @class Service
 */
class Service {
    static requestConfig() {
        return {
            headers: {
                Authorization: `Bearer ${sessionStorage._sessionToken}`
            }
        };
    }

    static getData() {
        return axios.get(ENV.apiBase + '/posts', this.requestConfig());
    }

    static getItem(id) {
        return axios.get(ENV.apiBase + '/posts/' + id, this.requestConfig());
    }

    static saveItem(requestData) {
        return axios.put(ENV.apiBase + '/posts/' + requestData.id, requestData, this.requestConfig());
    }

    static createItem(requestData) {
        return axios.post(ENV.apiBase + '/posts', requestData, this.requestConfig());
    }
}

export default Service;